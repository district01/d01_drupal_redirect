# d01_drupal_redirect #

Provides redirect config entities that can redirect from a 404 path to an existing internal page.

# Note #

Since redirects are config entities, they are imported and exported as configuration.

Don't forget to add ```d01_drupal_redirect.d01_drupal_redirect.* ``` to config_ignore if you don't want the entities that are not exported as configuration to be overwritten. 