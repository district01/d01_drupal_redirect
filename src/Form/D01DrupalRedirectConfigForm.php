<?php

/**
 * @file
 * D01 redirect config form.
 *
 * This config form is currently not being used because we haven't got
 * an autocomplete that supports multiple entity types.
 *
 * However, when this autocomplete is ready; we can use this form to
 * create a list of entity types.
 *
 * @todo Use this config form when autocomplete functionality is ready.
 */

namespace Drupal\d01_drupal_redirect\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Class RedirectConfigForm.
 *
 * @package Drupal\d01_drupal_redirect
 */
class D01DrupalRedirectConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManager $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'd01_drupal_redirect_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('d01_drupal_redirect.config');

    $entity_definitions = $this->entityTypeManager->getDefinitions();

    $options = [];
    foreach ($entity_definitions as $entity_id => $entity_definition) {
      if ($entity_definition instanceof \Drupal\Core\Entity\ContentEntityTypeInterface) {
        $options[$entity_id] = $this->buildCheckboxLabel($entity_definition, $entity_id);
      }
    }

    $form['allowed_entities'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => $this->t('Allowed entity types'),
      '#description' => $this->t('The selected entity types will be allowed for redirection'),
      '#default_value' => $config->get('allowed_entities'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('d01_drupal_redirect.config');
    $config->set('allowed_entities', $form_state->getValue('allowed_entities'))->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'd01_drupal_redirect.config',
    ];
  }

  /**
   * Build the checkbox label.
   */
  private function buildCheckboxLabel($entity_definition, $entity_id) {
    return $entity_definition->getLabel() . ' (' . $entity_id . ')';
  }
}
