<?php

namespace Drupal\d01_drupal_redirect\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\d01_drupal_redirect\D01DrupalRedirectService;
use Drupal\d01_drupal_redirect\Entity\D01DrupalRedirect;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RedirectForm.
 */
class D01DrupalRedirectForm extends EntityForm {

  /**
   * The redirect service.
   *
   * @var \Drupal\d01_drupal_redirect\D01DrupalRedirectService
   */
  protected $redirectService;

  /**
   * Constructor.
   *
   * @param \Drupal\d01_drupal_redirect\D01DrupalRedirectService $redirect_service
   *   The redirect service.
   */
  public function __construct(D01DrupalRedirectService $redirect_service) {
    $this->redirectService = $redirect_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('d01_drupal_redirect.redirect')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $redirect = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $redirect->label(),
      '#description' => $this->t("Label for the redirect."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $redirect->id(),
      '#machine_name' => [
        'exists' => '\Drupal\d01_drupal_redirect\Entity\D01DrupalRedirect::load',
      ],
      '#disabled' => !$redirect->isNew(),
    ];

    $form['from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect from'),
      '#maxlength' => 255,
      '#default_value' => $redirect->from(),
      '#description' => $this->t("The path to redirect. Requires the following format: /example"),
      '#required' => TRUE,
    ];

    // @todo Create a custom autocomplete controller that accepts multiple entity types.
    // For now, support the 'node' entity type only.
    $form['to'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => $this->t('Redirect to'),
      '#maxlength' => 255,
      '#default_value' => $redirect->getNodeToRedirectTo(),
      '#description' => $this->t("The internal path to redirect to."),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $redirect = $this->entity;
    $status = $redirect->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label redirect.', [
          '%label' => $redirect->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label redirect.', [
          '%label' => $redirect->label(),
        ]));
    }
    $form_state->setRedirectUrl($redirect->toUrl('collection'));
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $uri = $form_state->getValue('from');

    $original_id = $this->entity->getOriginalId();
    $original_redirect = $original_id ? D01DrupalRedirect::load($original_id) : FALSE;

    if (!$original_redirect || $original_redirect->from() !== $this->entity->from()) {
      if ($this->redirectService->getMatchingRedirect(trim($uri))) {
        $form_state->setError($form['from'], t('This redirect path already exists.'));
      }
    }

    if (preg_match('#^/#', $uri) !== 1) {
      $form_state->setError($form['from'], t('This redirect path is invalid. It should start with a slash ( / ).'));
    }
  }
}
