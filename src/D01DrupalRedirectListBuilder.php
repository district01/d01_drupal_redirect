<?php

namespace Drupal\d01_drupal_redirect;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Redirect entities.
 */
class D01DrupalRedirectListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Redirect');
    $header['id'] = $this->t('Machine name');
    $header['from'] = $this->t('From');
    $header['to'] = $this->t('To');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['from'] = $entity->from();

    $node = $entity->getNodeToRedirectTo();
    $row['to'] = '/';
    if ($node) {
      $row['to'] = $node->label();
    }

    return $row + parent::buildRow($entity);
  }

}
