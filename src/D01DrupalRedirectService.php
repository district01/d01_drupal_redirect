<?php

namespace Drupal\d01_drupal_redirect;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Redirect service.
 *
 * @package Drupal\d01_drupal_redirect
 */
class D01DrupalRedirectService  implements D01DrupalRedirectServiceInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getMatchingRedirect($uri) {
    return $this->entityTypeManager->getStorage('d01_drupal_redirect')->getQuery()
      ->condition('from', $uri)
      ->condition('status', 1)
      ->execute();
  }
}
