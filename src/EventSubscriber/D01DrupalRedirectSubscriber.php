<?php

namespace Drupal\d01_drupal_redirect\EventSubscriber;

use Drupal\Core\Url;
use Drupal\d01_drupal_redirect\Entity\D01DrupalRedirect;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\d01_drupal_redirect\D01DrupalRedirectService;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * Class D01DrupalRedirectSubscriber.
 *
 * @package Drupal\d01_drupal_redirect\EventSubscriber
 */
class D01DrupalRedirectSubscriber implements EventSubscriberInterface {

  /**
   * The redirect content service.
   *
   * @var \Drupal\d01_drupal_redirect\D01DrupalRedirectService
   */
  protected $redirectService;

  /**
   * RedirectSubscriber constructor.
   *
   * @param \Drupal\d01_drupal_redirect\D01DrupalRedirectService $redirect_service
   *   The group content service.
   */
  public function __construct(D01DrupalRedirectService $redirect_service) {
    $this->redirectService = $redirect_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return ([
      KernelEvents::EXCEPTION => [
        ['redirect'],
      ],
    ]);
  }

  /**
   * Perform redirect for http request.
   *
   * @param GetResponseForExceptionEvent $event
   *   The exception event.
   */
  public function redirect(GetResponseForExceptionEvent $event) {
    $exception = $event->getException();
    if (!$exception || !$exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
      return;
    }

    $status_code = $exception->getStatusCode();
    if ($status_code !== 404) {
      return;
    }

    $request_uri = $event->getRequest()->getRequestUri();

    $matching_redirect_ids = $this->redirectService->getMatchingRedirect($request_uri);

    // If there are multiple matches, pick the first suitable one.
    $redirect_url = FALSE;
    foreach ($matching_redirect_ids as $matching_redirect_id) {
      if (!$matching_redirect_id) {
        continue;
      }

      $redirect = D01DrupalRedirect::load($matching_redirect_id);
      if (!$redirect) {
        continue;
      }

      $node = $redirect->getNodeToRedirectTo();
      if (!$node) {
        continue;
      }

      $redirect_url = $url_object = Url::fromRoute('entity.node.canonical', [
        'node' => $node->id(),
      ]);
    }

    if (!$redirect_url) {
      return;
    }

    // If there are no matching urls, this won't be executed
    // and we show the 404 anyway.
    $event->setResponse(new TrustedRedirectResponse($redirect_url->toString(), 301));
  }
}
