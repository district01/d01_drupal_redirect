<?php

namespace Drupal\d01_drupal_redirect;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * D01 Redirect service.
 *
 * @package Drupal\d01_drupal_redirect
 */
interface D01DrupalRedirectServiceInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager);

  /**
   * Get matching redirect results.
   *
   * @param string $uri
   *   The uri to search for.
   *
   * @return array $results
   *   The matching redirect results.
   */
  public function getMatchingRedirect($uri);
}
