<?php

namespace Drupal\d01_drupal_redirect;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for redirect entities.
 */
class D01DrupalRedirectHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    return parent::getRoutes($entity_type);
  }

}
