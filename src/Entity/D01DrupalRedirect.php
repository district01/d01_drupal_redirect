<?php

namespace Drupal\d01_drupal_redirect\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\node\Entity\Node;

/**
 * Defines the d01 redirect entity.
 *
 * @ConfigEntityType(
 *   id = "d01_drupal_redirect",
 *   label = @Translation("Redirect"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\d01_drupal_redirect\D01DrupalRedirectListBuilder",
 *     "form" = {
 *       "add" = "Drupal\d01_drupal_redirect\Form\D01DrupalRedirectForm",
 *       "edit" = "Drupal\d01_drupal_redirect\Form\D01DrupalRedirectForm",
 *       "delete" = "Drupal\d01_drupal_redirect\Form\D01DrupalRedirectDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\d01_drupal_redirect\D01DrupalRedirectHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "d01_drupal_redirect",
 *   admin_permission = "administer d01 redirect",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "from" = "from",
 *     "to" = "to",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/d01_drupal_redirect/{d01_drupal_redirect}",
 *     "add-form" = "/admin/structure/d01_drupal_redirect/add",
 *     "edit-form" = "/admin/structure/d01_drupal_redirect/{d01_drupal_redirect}/edit",
 *     "delete-form" = "/admin/structure/d01_drupal_redirect/{d01_drupal_redirect}/delete",
 *     "collection" = "/admin/structure/d01_drupal_redirect"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "from",
 *     "to",
 *   }
 * )
 */
class D01DrupalRedirect extends ConfigEntityBase implements D01DrupalRedirectInterface {

  /**
   * The Redirect ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Redirect label.
   *
   * @var string
   */
  protected $label;

  /**
   * The path to redirect.
   *
   * @var string
   */
  protected $from;

  /**
   * The path to redirect to.
   *
   * @var string
   */
  protected $to;

  /**
   * {@inheritdoc}
   */
  public function from() {
    return $this->from ?: FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function to() {
    return $this->to ?: FALSE;
  }

  /**
   * Get the node to redirect to.
   *
   * @return bool|Node $node
   *   The node object.
   */
  public function getNodeToRedirectTo() {
    $id = $this->to();
    if (!$id) {
      return FALSE;
    }

    return Node::load($id);
  }
}
