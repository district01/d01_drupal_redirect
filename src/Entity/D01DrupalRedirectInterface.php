<?php

namespace Drupal\d01_drupal_redirect\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\node\Entity\Node;

/**
 * Provides an interface for defining Redirect entities.
 */
interface D01DrupalRedirectInterface extends ConfigEntityInterface {

  /**
   * Get the path to redirect.
   *
   * @return string $path
   *   A string that represents a path alias.
   */
  public function from();

  /**
   * Get the path to redirect to.
   *
   * @return Node $node
   *   The node to redirect to.
   */
  public function to();
}
